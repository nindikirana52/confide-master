package com.confide.confide.model.data

data class Reply(
    var id: String,
    var reply: String,
    var time: String,
    var deskripsi: String,
    var idUser: String,
    var idPostingan: String,
    var jenis: String
)
