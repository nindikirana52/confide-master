package com.confide.confide.model.repo

import com.confide.confide.model.data.Login

interface User {
    fun login(nim: String, password: String, onSuccess: (Login) -> Unit, onError: (String) -> Unit)
}