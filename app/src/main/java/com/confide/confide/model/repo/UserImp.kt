package com.confide.confide.model.repo

import com.confide.confide.model.data.Login
import com.confide.confide.remote.BASEURL
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserImp(var base: BASEURL) : User {
    override fun login(
        nim: String,
        password: String,
        onSuccess: (Login) -> Unit,
        onError: (String) -> Unit
    ) {
        base.api.login(nim, password).enqueue(object : Callback<Login> {
            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.isSuccessful) {
                    if (response.code() == 200) {
                        onSuccess(response.body()!!)
                    } else {
                        onError("${response.code()} ${response.message()}")
                    }
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                onError(t.toString())
            }

        })
    }

}