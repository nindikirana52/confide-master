package com.confide.confide.model.repo

import android.util.Log
import com.confide.confide.model.data.Post
import com.google.firebase.auth.ActionCodeSettings
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class FirebaseImp : FirebaseData {
    override fun verifikasi(email: String, onSuccess: () -> Unit, onError: (String) -> Unit) {
        val actionCodeSettings =
            ActionCodeSettings.newBuilder() // URL you want to redirect back to. The domain (www.example.com) for this
                // URL must be whitelisted in the Firebase Console.
                .setUrl("https://www.example.com/finishSignUp?cartId=1234")
                .setHandleCodeInApp(true)
                .setAndroidPackageName(
                    "com.confide.confide",
                    true,  /* installIfNotAvailable */
                    "12" /* minimumVersion */
                )
                .build()
        val auth = FirebaseAuth.getInstance()
        auth.sendSignInLinkToEmail(email, actionCodeSettings)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("EmailVerif", "Email sent.")
                    onSuccess()
                }
                else{
                    onError(task.exception.toString())
                    Log.d("EmailVerif","Error")
                }
            }
//        var mAuthListener = object : FirebaseAuth.AuthStateListener {
//            override fun onAuthStateChanged(p0: FirebaseAuth) {
//                var user = p0.currentUser
//                if (user != null) {
//                    sendEmailVerification()
//                } else {
//                    Log.d("User Status", "SignOut")
//                }
//            }
//        }
    }

    private fun sendEmailVerification(email:String) {
//        var user = FirebaseAuth.getInstance().currentUser
//        user?.sendEmailVerification()?.addOnCompleteListener(object : OnCompleteListener<Void> {
//            override fun onComplete(p0: Task<Void>) {
//                if (p0.isSuccessful) {
//                    FirebaseAuth
//                }
//            }
//
//        })
//        var actionCodeSettings:ActionCodeSettings

    }

    override fun getPost(onSuccess: (List<Post>) -> Unit, onError: (String) -> Unit) {
        FirebaseDatabase.getInstance().reference.child("Post").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    override fun sendPost(post: Post) {
    }

}