package com.confide.confide.model.data

class Komentar(
    var id: String,
    var jenis: String,
    var postingan: String,
    var time: String,
    var deskripsi: String,
    var idUser: String,
    var idPost: String
) {
    constructor() : this("", "", "", "", "", "", "")
}