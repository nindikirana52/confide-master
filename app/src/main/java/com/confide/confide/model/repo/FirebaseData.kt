package com.confide.confide.model.repo

import com.confide.confide.model.data.Post

interface FirebaseData {
    fun verifikasi(email: String, onSuccess: () -> Unit, onError: (String) -> Unit)
    fun getPost(onSuccess: (List<Post>) -> Unit, onError: (String) -> Unit)
    fun sendPost(post:Post)
}