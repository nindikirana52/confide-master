package com.confide.confide.model.data

import com.google.gson.annotations.SerializedName

data class Login(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("expertise")
	val expertise: List<ExpertiseItem?>? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)

data class ExpertiseItem(

	@field:SerializedName("user_code")
	val userCode: String? = null,

	@field:SerializedName("expertise")
	val expertise: String? = null
)

data class Data(

	@field:SerializedName("user_email")
	val userEmail: String? = null,

	@field:SerializedName("rt")
	val rt: String? = null,

	@field:SerializedName("user_image")
	val userImage: String? = null,

	@field:SerializedName("rw")
	val rw: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("jurusan")
	val jurusan: String? = null,

	@field:SerializedName("user_cd")
	val userCd: String? = null,

	@field:SerializedName("fakultas")
	val fakultas: String? = null,

	@field:SerializedName("province")
	val province: String? = null,

	@field:SerializedName("user_npm")
	val userNpm: String? = null,

	@field:SerializedName("subdistrict")
	val subdistrict: String? = null,

	@field:SerializedName("district")
	val district: String? = null,

	@field:SerializedName("village")
	val village: String? = null,

	@field:SerializedName("no_telp")
	val no_telp: String? = null,

	@field:SerializedName("user_description")
	val userDescription: String? = null
)
