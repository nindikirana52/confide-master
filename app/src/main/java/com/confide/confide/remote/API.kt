package com.confide.confide.remote

import com.confide.confide.model.data.Login
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
    @GET("auth/user")
    fun login(
        @Query("npm") npm: String,
        @Query("password") password: String
    ): Call<Login>
}