package com.confide.confide.presenter

import com.confide.confide.model.data.Login
import com.confide.confide.model.repo.UserImp

class LoginPresenter(var userImp: UserImp, var view: LoginView) {
    fun login(nim: String, password: String) {
        userImp.login(nim, password, {
            view.onSuccess(it)
        }, {
            view.onError(it)
        })
    }
}

interface LoginView {
    fun onSuccess(it: Login)
    fun onError(it: String)

}