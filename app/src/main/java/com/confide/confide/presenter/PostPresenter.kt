package com.confide.confide.presenter

import android.content.Intent
import android.graphics.Bitmap
import android.widget.Toast
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.model.data.Post
import com.confide.confide.ui.view.MainActivity
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_post.*
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class PostPresenter(val view: PostView) {
    fun uploadPost(desc: String, bitmap: Bitmap) {
        var key = System.currentTimeMillis().toString()
        var db = FirebaseDatabase.getInstance().reference.child(
            "Post/${key}"
        )
        val firebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
        var storageReference: StorageReference = firebaseStorage.getReference()
        var imageRef: StorageReference =
            storageReference.child("Post/images/" + key + ".jpg")
        val bao = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 50, bao)
        val data = bao.toByteArray();
        val uploadTask: UploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            view.onError(it.toString())
        }.addOnSuccessListener {
            imageRef.downloadUrl.addOnSuccessListener {
                val locale = Locale("in", "ID")
                val simpleFormatDate = SimpleDateFormat("dd-MM-yyyy HH:mm", locale)
                val tanggal = simpleFormatDate.format(Calendar.getInstance().time)
                val post = Post(
                    key,
                    "image",
                    it.toString(),
                    tanggal,
                    desc,
                    SharedPrefUtil.getString("NIM")!!
                )
                db.setValue(post).addOnSuccessListener {
                    view.onSuccess()
                }
                    .addOnFailureListener {
                        view.onError(it.toString())
                    }
            }
        }
    }

    interface PostView {
        fun onError(toString: String)
        fun onSuccess()

    }
}