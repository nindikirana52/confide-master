package com.confide.confide.presenter

import com.confide.confide.model.repo.FirebaseImp

class VerifikasiPresenter(var imp:FirebaseImp,var view:VerifikasiView) {
    fun verifikasi(email:String){
        imp.verifikasi(email,{
            view.onSuccess()
        },{
            view.onError(it)
        })
    }
}
interface VerifikasiView{
    fun onSuccess()
    fun onError(it: String)

}