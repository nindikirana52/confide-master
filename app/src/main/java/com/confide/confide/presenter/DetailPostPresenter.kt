package com.confide.confide.presenter

import com.confide.confide.model.data.Komentar
import com.confide.confide.model.data.Like
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DetailPostPresenter(val view: DetaiPostView) {
    fun getKomentar(idPost: String) {

        var db = FirebaseDatabase.getInstance().reference.child("Post")
        db.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                view.onError(error.toString())
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                val listKey: ArrayList<String> = ArrayList()
                val listKomentar: ArrayList<Komentar> = ArrayList()
                val listLike: ArrayList<Like> = ArrayList()
                for (i in snapshot.children) {
                    val key: String = i.key!!
                    val like = i.child("like").getValue(Like::class.java)
                    if (like != null) {
                        listLike.add(like!!)
                    }
                    listKey.add(key)
                    val komentar = i.getValue(Komentar::class.java)
                    listKomentar.add(komentar!!)
                }
                view.onSuccess(listKomentar, listKey, listLike)

            }

        })
    }
}

interface DetaiPostView {
    fun onError(toString: String)
    fun onSuccess(
        listKomentar: java.util.ArrayList<Komentar>,
        listKey: java.util.ArrayList<String>,
        listLike: ArrayList<Like>
    )

}