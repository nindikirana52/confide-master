package com.confide.confide.presenter

import com.confide.confide.model.data.Login
import com.confide.confide.model.repo.UserImp

class ProfilePresenter(var userImp: UserImp, var view: ProfileView) {
    fun profile(nim: String, password: String) {
        userImp.login(nim, password, {
            view.onSuccess(it)
        }, {
            view.onError(it)
        })
    }
}

interface ProfileView {
    fun onSuccess(it: Login)
    fun onError(it: String)

}