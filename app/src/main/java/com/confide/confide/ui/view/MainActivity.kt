package com.confide.confide.ui.view

import android.database.DatabaseUtils
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import com.confide.confide.R
import androidx.navigation.findNavController
import com.confide.confide.ui.view.fragment.HomeFragment
import com.confide.confide.ui.view.fragment.PostFragment
import com.confide.confide.ui.view.fragment.ProfileFragment
import com.confide.confide.ui.view.fragment.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window!!.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        init()
        navigate(HomeFragment())
    }

    private fun init() {
        bottomNav.setOnNavigationItemSelectedListener{
            when(it.itemId){
                R.id.ic_home -> navigate(HomeFragment())
                R.id.ic_post -> navigate(PostFragment())
                R.id.ic_trending -> {
                    Toast.makeText(applicationContext,"Coming soon", Toast.LENGTH_SHORT).show()

                    true
                }
                R.id.ic_profile -> navigate (ProfileFragment())
                R.id.ic_search -> navigate (SearchFragment())
                else -> false
            }
        }
    }

    private fun navigate(id: Fragment): Boolean{
        supportFragmentManager.beginTransaction().replace(R.id.frame_main,id).commit()
        return true
    }
}