package com.confide.confide.ui.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.arch.core.executor.TaskExecutor
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.model.data.UserFirebase
import com.confide.confide.model.repo.FirebaseImp
import com.confide.confide.presenter.VerifikasiPresenter
import com.confide.confide.presenter.VerifikasiView
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.TaskExecutors
import com.google.android.gms.tasks.TaskExecutors.MAIN_THREAD
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import kotlinx.android.synthetic.main.activity_verifikasi.*
import java.lang.Exception
import java.util.concurrent.TimeUnit


class VerifikasiActivity : AppCompatActivity(), VerifikasiView {
    private lateinit var noTelp: String
    lateinit var presenter: VerifikasiPresenter
    lateinit var firebaseAuth: FirebaseAuth
    var mIdVerifikasi: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verifikasi)
//        initPresenter()
        initFirebase()
        if (intent.getStringExtra("noTelp")!!
                .isEmpty() || intent.getStringExtra("noTelp") == null
        ) {
            Toast.makeText(
                applicationContext,
                "Tidak dapat login\nNomor Telepon Kosong",
                Toast.LENGTH_SHORT
            ).show()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            var temp_noTelp = "${intent.getStringExtra("noTelp")!!}"
            if (temp_noTelp.substring(0, 1).equals("0") || temp_noTelp.substring(0, 1)
                    .equals("O") || temp_noTelp.substring(0, 1).equals("o")
            ) {
                noTelp = "+62${temp_noTelp.substring(1, temp_noTelp.length)}"
            } else {
                noTelp = "+${intent.getStringExtra("noTelp")!!}"
            }
            Log.d("NOTELPON", noTelp!!)
            kirimVerifikasi(noTelp!!)
            kirimUlang()
            tvNoTelponVerifikasi.setText("Kode verifikasi telah dikirim ke $noTelp")
            btVerifikasi.setOnClickListener {
            }
        }
    }

    private fun kirimUlang() {
        val delay = 1000
        var durasi = 60
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                durasi = durasi - 1
                tvKirimUlang.setText(durasi.toString())
                if (durasi == 0) {
                    tvKirimUlang.setText("Kirim Ulang")
                    tvKirimUlang.setTextColor(resources.getColor(R.color.colorPrimary))
                    tvKirimUlang.isEnabled = true
                    tvKirimUlang.setOnClickListener {
                        kirimVerifikasi(noTelp!!)
                        Toast.makeText(applicationContext, "Mengirim Ulang...", Toast.LENGTH_SHORT)
                            .show()
                        durasi = 60
                        handler.postDelayed(this, 1000)
                    }
                } else {
                    tvKirimUlang.setTextColor(resources.getColor(android.R.color.darker_gray))
                    tvKirimUlang.isEnabled = false
                    handler.postDelayed(this, delay.toLong())
                }
            }

        }, delay.toLong())
    }

    private fun initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance()
    }

    private fun kirimVerifikasi(noTelp: String) {
        val options = PhoneAuthOptions.newBuilder(firebaseAuth)
            .setPhoneNumber(noTelp)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(mCallback)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(
            options
        )
    }

    private val mCallback = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(p0: PhoneAuthCredential) {
            var kode = p0.smsCode.toString()
            try {
                etKodeVerifikasi.setText(kode)
                verifikasiKode(kode)
            } catch (e: Exception) {

                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        override fun onVerificationFailed(p0: FirebaseException) {
            Toast.makeText(applicationContext, p0.toString(), Toast.LENGTH_SHORT).show()

        }

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)
            mIdVerifikasi = p0
        }

    }

    private fun verifikasiKode(kode: String) {
        try {
            val credential: PhoneAuthCredential =
                PhoneAuthProvider.getCredential(mIdVerifikasi!!, kode)
            login(credential)
        } catch (e: Exception) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    private fun login(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(
            this, {
                if (it.isSuccessful) {
                    getToken()
                    pushFirebase(it.result!!.user!!.uid)
                    SharedPrefUtil.saveBoolean("login", true)
                    SharedPrefUtil.saveString("id", it.result!!.user!!.uid)

                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(applicationContext, "Kode Salah", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun getToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(object :
            OnSuccessListener<InstanceIdResult> {
            override fun onSuccess(p0: InstanceIdResult?) {
                SharedPrefUtil.saveString("token", p0!!.token)
                pushToken()
            }
        })
    }

    private fun pushToken() {
        val map = HashMap<String, Any>()
        map.put("token", SharedPrefUtil.getString("token")!!)
        FirebaseDatabase.getInstance().reference.child("Tokens/${this.intent.getStringExtra("nip")}")
            .updateChildren(map)
    }

    private fun pushFirebase(uid: String) {
        val userFirebase = UserFirebase()
        userFirebase.nama = this.intent.getStringExtra("nama")!!
        userFirebase.noTelp = this.intent.getStringExtra("noTelp")!!
        userFirebase.uid = uid
        FirebaseDatabase.getInstance().reference.child("User/${SharedPrefUtil.getString("NIM")}")
            .setValue(userFirebase)
    }
//
//    fun initPresenter() {
//        presenter = VerifikasiPresenter(FirebaseImp(), this)
//        presenter.verifikasi(intent.getStringExtra("email") ?: "gustumaulanaf@gmail.com")
//    }

    override fun onSuccess() {
        Toast.makeText(applicationContext, "Berhasil diverifikasi", Toast.LENGTH_SHORT).show()
    }

    override fun onError(it: String) {
        Toast.makeText(applicationContext, "$it", Toast.LENGTH_SHORT).show()

    }
}