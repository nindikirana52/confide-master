package com.confide.confide.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil

class SplashScreenActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler(Looper.getMainLooper()).postDelayed({
            if (SharedPrefUtil.getString("token") != null || !SharedPrefUtil.getString("token")!!
                    .isEmpty()
            ) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                startActivity(Intent(this, SliderActivity::class.java))
                finish()
            }
        }, 3000)
    }
}