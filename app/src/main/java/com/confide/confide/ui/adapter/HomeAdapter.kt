package com.confide.confide.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.confide.confide.R
import com.confide.confide.model.data.Like
import com.confide.confide.model.data.Post
import com.confide.confide.ui.view.DetailPost
import kotlinx.android.synthetic.main.item_home.view.*

class HomeAdapter(
    var list: List<Post>,
    var listKey: ArrayList<String>,
    var listLike: ArrayList<Like>,
    var c: Context
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgUsr = itemView.imgUserHome
        val imgPost = itemView.imgPostHome
        val desc = itemView.tvDeskripsiPostHome
        val time = itemView.tvTimeHome
        val like = itemView.tvTotalLike
        val comment = itemView.tvTotalComment
        val rate = itemView.tvRateHome
        val username = itemView.tvUsernameHome
        val more = itemView.imgMoreMenu
        val btComment = itemView.btComment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(c).load(
            "http://amikom.ac.id/public/fotomhs/2020/${
                list.get(position).idUser.replace(
                    ".",
                    "_"
                )
            }.jpg"
        ).into(holder.imgUsr)
        Glide.with(c).load(list.get(position).postingan).into(holder.imgPost)
        holder.time.setText(list.get(position).time)
        holder.desc.setText(list.get(position).deskripsi)
        holder.like.setText("10")
        holder.comment.setText("12")
        holder.username.setText(list.get(position).idUser)
        holder.rate.setText("4.5/5")
        holder.more.setOnClickListener {
            var popMenu = PopupMenu(c,it)
            popMenu.inflate(R.menu.more_menu)
            popMenu.setOnMenuItemClickListener {
                when(it.itemId){
                R.id.action_report -> {
                    Toast.makeText(c,"Berhasil di report",Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                    else -> {
                        return@setOnMenuItemClickListener true
                    }
                }
            }
            popMenu.show()

        }
        holder.btComment.setOnClickListener {
            val intent = Intent(c,DetailPost::class.java)
            intent.putExtra("idPost",listKey.get(position))
            c.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}