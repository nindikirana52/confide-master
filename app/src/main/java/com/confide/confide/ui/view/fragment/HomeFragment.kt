package com.confide.confide.ui.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.confide.confide.R
import com.confide.confide.model.data.Like
import com.confide.confide.model.data.Post
import com.confide.confide.ui.adapter.HomeAdapter
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {
    private lateinit var mContext: Context
    var listLike: ArrayList<Like> = ArrayList()
    var listComment: ArrayList<Like> = ArrayList()
    var listKey: ArrayList<String> = ArrayList()
    var listPostingan: ArrayList<Post> = ArrayList()
    lateinit var root: View
    lateinit var db: DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_home, container, false)
        val intent = Intent()
        root.toolbarHome.setSubtitle(intent.getStringExtra("nama"))
        initFirebase()
        return root
    }

    private fun initFirebase() {
        db = FirebaseDatabase.getInstance().reference.child("Post")
        db.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "${error}", Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                listPostingan.clear()
                listKey.clear()
                listLike.clear()
                listComment.clear()
                for (i in snapshot.children) {
                    val key: String = i.key!!
                    val like = i.child("like").getValue(Like::class.java)
                    if (like != null) {
                        listLike.add(like!!)
                    }
                    listKey.add(key)
                    val postingan = i.getValue(Post::class.java)
                    listPostingan.add(postingan!!)
                }
                var adapter =
                    HomeAdapter(listPostingan, listKey, listLike, mContext)
                val linearLayoutManager = LinearLayoutManager(mContext!!)
                linearLayoutManager.reverseLayout = true
                linearLayoutManager.stackFromEnd = true
                root.rvPostingan.layoutManager = linearLayoutManager
                root.rvPostingan.setHasFixedSize(true)
                root.rvPostingan.adapter = adapter
                adapter.notifyDataSetChanged()
            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
}