package com.confide.confide.ui.view

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.ui.adapter.SliderAdapter
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_slider.*

class SliderActivity : AppCompatActivity() {

    val listImage = arrayOf(R.drawable.login, R.drawable.ic_comment, R.drawable.ic_report)
    val listPetunjuk = arrayOf(
            "Rule of Login",
            "Rule of Post and Comment",
            "Rule of Report")
    val listDeskripsiJudul = arrayOf(
            "Login menggunakan nim dan password yang terdaftar pada Universitas.",
            "Dilarang menggunakan kata yang mengandung unsur pornografi, sara, rasisme, jual-beli, dan spam.",
            "Bagi yang melanggar aturan aplikasi maka akan mendapat tambahan poin pelanggaran.")

    lateinit var sliderAdapter: SliderAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (SharedPrefUtil.getBoolean("dark_mode")) {
            setTheme(R.style.NoBar)
        }
        setContentView(R.layout.activity_slider)
        if (SharedPrefUtil.getBoolean("dark_mode")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.white)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window!!.decorView.systemUiVisibility = 0
            }
        }
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.white)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window!!.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        sliderAdapter = SliderAdapter(this, listImage, listPetunjuk, listDeskripsiJudul)
        sliderTutorial.setIndicatorAnimation(IndicatorAnimations.WORM)
        sliderTutorial.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderTutorial.isAutoCycle = false
        sliderTutorial.sliderAdapter = sliderAdapter
    }
}