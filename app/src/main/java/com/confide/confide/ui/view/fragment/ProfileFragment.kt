package com.confide.confide.ui.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.model.data.ExpertiseItem
import com.confide.confide.model.data.Login
import com.confide.confide.model.repo.UserImp
import com.confide.confide.presenter.LoginPresenter
import com.confide.confide.presenter.ProfilePresenter
import com.confide.confide.presenter.ProfileView
import com.confide.confide.remote.BASEURL
import com.confide.confide.ui.adapter.ExpertiseAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment(), ProfileView {
    lateinit var presenter: ProfilePresenter
    lateinit var root: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_profile, container, false)
        initPresenter()
        root.swipeProfile.setOnRefreshListener {
            initPresenter()
            root.swipeProfile.isRefreshing = false
        }
        root.tabProfile.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                Toast.makeText(requireContext(),"Coming soon",Toast.LENGTH_SHORT).show()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })
        return root

    }

    private fun initPresenter() {
        presenter = ProfilePresenter(UserImp(BASEURL()), this)
        presenter.profile(SharedPrefUtil.getString("NIM")!!, SharedPrefUtil.getString("PASSWORD")!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onSuccess(it: Login) {
        root.tvUsernameProfile.setText(it.data?.userName)
        root.tvNimProfile.setText(it.data?.userNpm)
        root.tvAboutMeProfile.setText(it.data?.userDescription)
        root.tvAboutMeProfile.setText(it.data?.userDescription)
        root.tvUserFacultyProfile.setText(it.data?.fakultas)
        root.tvEmailProfile.setText(it.data?.userEmail)
        root.tvMajorProfile.setText(it.data?.jurusan)
        root.tvRt.setText(it.data?.rt)
        root.tvRw.setText(it.data?.rw)
        root.tvVillage.setText(it.data?.village)
        root.tvSubDistrict.setText(it.data?.subdistrict)
        root.tvDistrict.setText(it.data?.district)
        root.tvProvince.setText(it.data?.province)
        Glide.with(requireContext()).load(it.data?.userImage).into(root.imgProfile)
        val adapter = ExpertiseAdapter(it.expertise as List<ExpertiseItem>)
        root.rvExpertise.adapter = adapter
        root.rvExpertise.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        root.rvExpertise.hasFixedSize()
    }

    override fun onError(it: String) {
        Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
    }
}