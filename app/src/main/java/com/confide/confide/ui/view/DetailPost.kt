package com.confide.confide.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.confide.confide.R
import com.confide.confide.model.data.Komentar
import com.confide.confide.model.data.Like
import com.confide.confide.presenter.DetaiPostView
import com.confide.confide.ui.adapter.KomentarAdapter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_detail_post.*
import kotlin.collections.ArrayList

class DetailPost : AppCompatActivity(), DetaiPostView {
    lateinit var dbPost:DatabaseReference
    lateinit var idPost: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_post)
        idPost = intent.getStringExtra("idPost") ?: ""
        initDB()
    }

    private fun initDB() {
        dbPost = FirebaseDatabase.getInstance().reference.child("Post/")
    }

    override fun onError(toString: String) {
        Toast.makeText(applicationContext, toString, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess(
        listKomentar: ArrayList<Komentar>,
        listKey: ArrayList<String>,
        listLike: ArrayList<Like>
    ) {
        val komentarAdapter = KomentarAdapter(listKomentar, listKey, listLike, this)
        rvKomentar.layoutManager = LinearLayoutManager(this)
        rvKomentar.hasFixedSize()
        rvKomentar.adapter = komentarAdapter
    }
}