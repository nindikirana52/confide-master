package com.confide.confide.ui.view.fragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.format.Time
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.helper.imageutil.ImageRotator
import com.confide.confide.model.data.Post
import com.confide.confide.presenter.PostPresenter
import com.confide.confide.ui.view.MainActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_post.*
import kotlinx.android.synthetic.main.fragment_post.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class PostFragment : Fragment(), PostPresenter.PostView {
    private lateinit var mActivity: Activity
    private lateinit var mContext: Context
    private val REQUEST_WRITE_STORAGE_REQUEST_CODE: Int = 1
    private var mHighQualityImageUri: Uri? = null
    private val REQUEST_GALERI: Int = 0
    private var bitmapFinal: Bitmap? = null
    lateinit var root: View
    lateinit var db: DatabaseReference
    lateinit var presenter: PostPresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_post, container, false)
        requestAppPermissions()
        initPresenter()
        root.img_send.setOnClickListener {
            if (!etPost.text!!.isEmpty()) {

                if (bitmapFinal != null) {
                    Toast.makeText(
                        mContext,
                        "Please wait...",
                        Toast.LENGTH_LONG
                    ).show()
                    presenter.uploadPost(etPost.text.toString(), bitmapFinal!!)
                } else {
                    Toast.makeText(
                        mContext,
                        "Untuk saat ini wajib melampirkan gambar",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            } else {
                Toast.makeText(
                    mContext,
                    "Untuk saat ini wajib melampirkan keterangan",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        root.img_foto.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, REQUEST_GALERI)
        }
        return root
    }

    private fun initPresenter() {
        presenter= PostPresenter(this)
    }

//    fun dialogCamera() {
//        val alertDialogBuilder = AlertDialog.Builder(this)
//        var view: View
//        view = layoutInflater.inflate(R.layout.item_ambil_foto, null)
//        alertDialogBuilder.setView(view)
//        view.layoutGaleri.setOnClickListener {
//
//        }
//        view.layoutKamera.setOnClickListener {
//            if (RequestCamera.checkPermission(applicationContext)) {
//                intentKamera()
//            } else {
//                RequestCamera.requestPermission(this)
//            }
//        }
//        alertDialog = alertDialogBuilder.create()
//        alertDialog.show()
//    }

//    private fun intentKamera() {
//        val builder = StrictMode.VmPolicy.Builder()
//        StrictMode.setVmPolicy(builder.build())
//        mHighQualityImageUri = generateTimeStampPhotoFileUri()
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri)
//        startActivityForResult(intent, REQUEST_KAMERA)
//    }

    private fun generateTimeStampPhotoFileUri(): Uri? {
        var photoFileUri: Uri? = null
        val code = mContext.packageManager.checkPermission(
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            mContext.packageName
        )
        if (code == PackageManager.PERMISSION_GRANTED) {
            val outputDir = getPhotoDirectory()
            if (outputDir != null) {
                val t = Time()
                t.setToNow()
                val photoFile = File(outputDir, System.currentTimeMillis().toString() + ".jpg")
                photoFileUri = Uri.fromFile(photoFile)
                return photoFileUri

            }

        } else {
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_SHORT).show()
        }
        return photoFileUri
    }

    private fun getPhotoDirectory(): File? {
        var outputDir: File? = null
        val externalStorageStagte = Environment.getExternalStorageState()
        if (externalStorageStagte == Environment.MEDIA_MOUNTED) {
            val photoDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            outputDir = File(photoDir, getString(R.string.app_name))
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(
                        mContext,
                        "Gagal Membuat Direktori " + outputDir.absolutePath,
                        Toast.LENGTH_SHORT
                    ).show()
                    outputDir = null
                }
        }
        return outputDir
    }

    private fun requestAppPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return
        }

        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), REQUEST_WRITE_STORAGE_REQUEST_CODE
        )
    }

    private fun hasReadPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun hasWritePermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GALERI && resultCode == Activity.RESULT_OK) {
            if (data!!.data != null) {
                val uri = data.data
                var inputStream: InputStream? = null
                try {
                    inputStream = requireActivity().contentResolver.openInputStream(uri!!)
                    val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                    val rotateBitmap: Bitmap =
                        ImageRotator.rotateImageIfRequired(bitmap, mContext, uri)
                    bitmapFinal = rotateBitmap
                    root.imgPost.setImageBitmap(bitmapFinal)

                } catch (e: Exception) {
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show()
                }
            }
        }




    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onError(toString: String) {
        Toast.makeText(mContext, toString, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess() {
        startActivity(Intent(mContext, MainActivity::class.java))
        mActivity?.finish()
    }
}