package com.confide.confide.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.ui.view.LoginActivity
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.item_slider_tutorial.view.*

class SliderAdapter(var activity: Activity, var listImage: Array<Int>, var  listPetunjuk: Array<String>, var listDeskripsiPetunjuk: Array<String>)
    : SliderViewAdapter<SliderAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val view: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_slider_tutorial, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, position: Int) {
        if (SharedPrefUtil.getBoolean("dark_mode")){
            viewHolder!!.layout.setBackgroundColor(ContextCompat.getColor(activity.applicationContext,R.color.black))
            viewHolder!!.title.setTextColor(ContextCompat.getColor(activity.applicationContext,R.color.white))
            viewHolder!!.description.setTextColor(ContextCompat.getColor(activity.applicationContext,R.color.white))
        }
        if (!position.equals(listDeskripsiPetunjuk.size -1)){
                viewHolder!!.btImgPetunjuk.visibility = View.GONE
            }else {
            viewHolder!!.btImgPetunjuk.visibility = View.VISIBLE
            viewHolder!!.btImgPetunjuk.setOnClickListener {
                val intent = Intent(activity.applicationContext, LoginActivity::class.java)
                activity.startActivity(intent)
                activity.finish()
            }
        }
        viewHolder.title.setText(listPetunjuk.get(position))
        viewHolder.description.setText(listDeskripsiPetunjuk.get(position))
        viewHolder.imgPetunjuk.setImageResource(listImage.get(position))
    }

    override fun getCount(): Int {
        return listDeskripsiPetunjuk.size
    }
    class ViewHolder(itemView: View?) : SliderViewAdapter.ViewHolder(itemView){
        val title = itemView!!.tvTitleItemPetunjuk
        val description = itemView!!.tvDeskripsiItemPetunjuk
        val imgPetunjuk = itemView!!.imgItemPetunjuk
        val btImgPetunjuk = itemView!!.btItemPetunjuk
        val layout = itemView!!.layout_slider_petunjuk
    }

}