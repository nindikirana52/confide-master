package com.confide.confide.ui.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.confide.confide.R
import com.confide.confide.model.data.ExpertiseItem
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.item_expertise.view.*

class ExpertiseAdapter  (var list:List<ExpertiseItem>)
    : RecyclerView.Adapter<ExpertiseAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text = itemView.tvExpertise
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_expertise, parent, false)
        return ExpertiseAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.setText(list.get(position).expertise)
    }

    override fun getItemCount(): Int {
        return list.size
    }

}

