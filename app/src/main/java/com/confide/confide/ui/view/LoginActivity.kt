package com.confide.confide.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.confide.confide.R
import com.confide.confide.helper.SharedPrefUtil
import com.confide.confide.model.data.Login
import com.confide.confide.model.repo.UserImp
import com.confide.confide.presenter.LoginPresenter
import com.confide.confide.presenter.LoginView
import com.confide.confide.remote.BASEURL
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {
    lateinit var presenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val button = findViewById<Button>(R.id.buttonLogin)
        initPresenter()
        button.setOnClickListener {
            if (Nim.text.toString().isEmpty()) {
                Nim.setError("Tidak boleh kosong")
            } else if (password.text.toString().isEmpty()) {
                password.setError("Tidak boleh kosong")
            } else {
                presenter.login(Nim.text.toString(), password.text.toString())
            }
        }
    }

    fun initPresenter() {
        presenter = LoginPresenter(UserImp(BASEURL()), this)
    }

    override fun onSuccess(it: Login) {
        SharedPrefUtil.saveString("NIM", Nim.text.toString())
        SharedPrefUtil.saveString("PASSWORD", password.text.toString())
        val intent = Intent(this, VerifikasiActivity::class.java)
        intent.putExtra("nama", it.data?.userName)
        intent.putExtra("noTelp", it.data?.no_telp)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onError(it: String) {
        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
    }
}